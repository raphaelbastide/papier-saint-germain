A pad to print for Beaux Arts de Paris students’, to create the school’s journal.

To add a new issue, add a new folder to `psg` with the number of the issue as name, you can copy paste one of the previous issues. You must then change the pads URLs in `index.html` and `pad.html` in order to link your new issue to the CSS and MD pads.