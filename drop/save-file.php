<?php
if (isset($_POST['fileData']) && isset($_POST['name'])) {
  $data = $_POST['fileData'];
  $name = $_POST['name'];
  // echo $data;
  $ext = explode('/', mime_content_type($data))[1];
  $data = str_replace('data:image/'.$ext.';base64,', '', $data);
  $data = str_replace(' ', '+', $data);
  $fileData = base64_decode($data);
  $filename = 'files/' . $name;
  $file = fopen($filename, 'wb');
  fwrite($file, $fileData);
  fclose($file);
  $urlBase = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
  echo 'document.querySelector(".path-line").value = "'.$urlBase. 'psg/drop/'. $filename.'";';
} else {
}
?>
